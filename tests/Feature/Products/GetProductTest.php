<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;

class GetProductTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGetListOfProductsWithoutAuth()
    {
        $response = $this->get('/products');
        $response->assertStatus(302);
    }
    public function testGetListOfProductsWithAuth()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->get('/products');
        $response->assertOk();
    }
}
